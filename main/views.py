from django.shortcuts import render, redirect
from django.http import JsonResponse
import json
import requests

# Create your views here.

def home (request):
    return render(request, 'home.html')

def searchView(request, key):
    result = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + key).json()
    return JsonResponse(result)

