from django.test import TestCase, Client
from django.urls import resolve, reverse


class UnitTesting (TestCase):
    def test0_urlHomeExist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'home.html')

    def test1_viewsTestJson(self):
        response = Client().get('/searchResult/test')
        self.assertContains(response, "https://www.googleapis.com")