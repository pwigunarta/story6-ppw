$(document).ready(function(){
    $.ajax({
        url: 'searchResult/html',
        success: function(data) { 
            let searchResult='';
                for(let i = 0; i < data.items.length; i++){
                    searchResult += "<tr> <th scope='row'> <img src='" + data.items[i].volumeInfo.imageLinks.thumbnail +"' class='img-thumbnail'></th>" +
                    "<td class='align-middle'>" + data.items[i].volumeInfo.title + "</td>" +
                    "<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" +
                    "<td class='align-middle'>" + data.items[i].volumeInfo.publisher + "</td>" +
                    "<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate + "</td> </tr>"
                }
            $('#results').html(searchResult);
        }
    })

    $('#searchBtn').on("click", function(event){
        var search = $('#searchIn').val();
        $.ajax({
            url: 'searchResult/' + search,
            success: function(data) { 
                let searchResult='';
                    for(let i = 0; i < data.items.length; i++){
                        searchResult += "<tr> <th scope='row'> <img src='" + data.items[i].volumeInfo.imageLinks.thumbnail +"' class='img-thumbnail'></th>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.title + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.publisher + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate + "</td> </tr>"
                    }
                $('#results').html(searchResult);
            }
        })
    })
})